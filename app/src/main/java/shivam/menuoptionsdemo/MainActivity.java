package shivam.menuoptionsdemo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ColorDrawable cd = new ColorDrawable(Color.parseColor("#00ff00"));
        getSupportActionBar().setBackgroundDrawable(cd);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inf = getMenuInflater();
        inf.inflate(R.menu.menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.a:
                Toast.makeText(MainActivity.this,"A",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.b:
                Toast.makeText(MainActivity.this,"B",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.c:
                Toast.makeText(MainActivity.this,"C",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.d:
                Toast.makeText(MainActivity.this,"D",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.e:
                Toast.makeText(MainActivity.this,"E",Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
